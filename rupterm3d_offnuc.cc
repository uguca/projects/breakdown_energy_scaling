#include <math.h>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>

#include "linear_shear_cohesive_law.hh"
#include "material.hh"
#include "precomputed_kernel.hh"
#include "static_communicator_mpi.hh"
#include "unimat_shear_interface.hh"
#include "uca_parameter_reader.hh"

using namespace uguca;

std::vector<std::string> &split(const std::string &s, char delim,
                                std::vector<std::string> &elems) {
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}

int main(int argc, char *argv[]) {
  int world_rank = StaticCommunicatorMPI::getInstance()->whoAmI();

  std::string fname;
  if(argc < 2) {
    if (world_rank == 0) {
      std::cerr << "Not enough arguments:"
                << " ./rupterm3d <input_file>" << std::endl;
    }
    return 0;
  } else {
    fname = argv[1];
  }

  ParameterReader data;
  data.readInputFile(fname);

  if (world_rank == 0) {
    std::cout << "simulation_code = weak-interface" << std::endl;
  }

  std::string bname = data.get<std::string>("simulation_name");
  std::string dump_folder = data.get<std::string>("dump_folder");
  if (world_rank == 0) {
    std::cout << "output_folder = " << dump_folder << std::endl;
  }

  // copy input file
  if (world_rank == 0) {
    std::ifstream src_ifile(fname, std::ios::binary);
    std::ofstream dst_ifile(dump_folder + bname + ".in", std::ios::binary);
    dst_ifile << src_ifile.rdbuf();
    src_ifile.close();
    dst_ifile.close();
  }

  double shear_load = data.get<double>("shear_load");   // 0;
  double normal_load = data.get<double>("normal_load"); //-10e6; // not needed

  double E = data.get<double>("E");     // 30.0e9;
  double nu = data.get<double>("nu");   // 0.25;
  double rho = data.get<double>("rho"); // 2655;

  double Gc = data.get<double>("Gc");       // 100.0;
  double tau_c = data.get<double>("tau_c"); // 80e6;
  double tau_r = data.get<double>("tau_r"); // 60e6;
  double a = data.get<double>("a");         // 0.30;
  double alpha = data.get<double>("alpha"); // 67.5e6;
  double beta = data.get<double>("beta");   // 75e6;

  double length = data.get<double>("length"); // 1;

  double nuc_half_size = data.get<double>("nuc_half_size"); // 3.8010e-02 ;
  double nuc_w_zone = data.get<double>("nuc_w_zone");       // 3.8010e-11;

  unsigned nb_nodes = data.get<unsigned>("nb_nodes"); // 512;

  double duration = data.get<double>("duration");                 // 7.5e-4;
  double time_step_factor = data.get<double>("time_step_factor"); // 0.35;

  double dump_interval = data.get<double>("dump_interval"); // 0;

  // ---------------------------------------------------------------------------

  // same number of nodes because node at x=length is the same as at x=0
  Mesh mesh(length, nb_nodes, length, nb_nodes);
  double dx = length / nb_nodes;
  double dz = length / nb_nodes;
  const std::vector<NodalField *> coords = mesh.getCoords();

  // constitutive interface law
  LinearShearCohesiveLaw law(mesh, Gc, tau_c, tau_r);
  NodalField * tau_c_p = law.getTauc();
  // NodalField * tau_r_p = law.getTaur();
  NodalField * G_c_p   = law.getGc();

  // materials
  Material mat = Material(E, nu, rho);
  mat.readPrecomputedKernels();
  if (world_rank == 0) {
    std::cout << "C_p = " << mat.getCp() << std::endl;
    std::cout << "C_s = " << mat.getCs() << std::endl;
    std::cout << "C_R = " << mat.getCr() << std::endl;
  }
  double nuc_speed = mat.getCr() * 0.1;

  // ---------------------------------------------------------------------------

  // weak interface
  UnimatShearInterface interface(mesh, mat, law);

  // external loading
  NodalField * ext_shear = interface.getShearLoad();
  NodalField * ext_normal = interface.getNormalLoad();
  // NodalField * ext_shear2 = interface.getLoad(2);
  ext_shear->setAllValuesTo(shear_load);
  ext_normal->setAllValuesTo(normal_load);
  for (int i = 0; i < mesh.getNbNodes(); ++i) {
    double x = std::abs((*coords[0])(i) - length / 2);
    double z = std::abs((*coords[2])(i) - length / 2);
    double r = std::sqrt(x * x + z * z);
    if (r <= a) {
      (*ext_shear)(i) = alpha;
    } else {
      (*ext_shear)(i) = std::max(alpha - beta * (r - a), 0.0);
    }
    // (*ext_shear2)(i) = (*ext_shear)(i) / std::sqrt(2);
    // (*ext_shear)(i) = (*ext_shear2)(i);
  }


  // time step
  double time_step = time_step_factor * interface.getStableTimeStep();
  // time_step = std::min(time_step, 1.0e-7);
  unsigned int nb_time_steps = duration / time_step;
  if (world_rank == 0) {
    std::cout << "time step     = " << time_step << std::endl;
    std::cout << "nb time steps = " << nb_time_steps << std::endl;
  }
  interface.setTimeStep(time_step);

  unsigned int dump_int = std::max(1, (int)(dump_interval / time_step));
  if (world_rank == 0)
    std::cout << "dump int = " << dump_int << std::endl;

  interface.initPredictorCorrector(1);
  interface.init();

  // dumping
  std::string dumper_bname = bname;
  std::string bname_sep = "-";
  std::string dumper_group_interface = "interface";
  if (world_rank == 0) {
    std::cout << "dumper_bname = " << dumper_bname << std::endl;
    std::cout << "bname_sep = " << bname_sep << std::endl;
    std::cout << "dumper_group = " << dumper_group_interface << std::endl;
  }
  interface.initDump(dumper_bname+bname_sep+dumper_group_interface,dump_folder, Dumper::Format::Binary);
  
  std::string dump_fields = data.get<std::string>("dump_fields");
  std::vector<std::string> dump_fields_sep = split(dump_fields,',');
  for (std::vector<std::string>::iterator it = dump_fields_sep.begin();
       it != dump_fields_sep.end(); ++it) {
    interface.registerDumpField(*it);
  }
  interface.dump(0,0);

  // ---------------------------------------------------------------------------
  // DENSE SCALAR DUMP
  std::string scalar_time_file_name = bname + "-interface.scalar_time";
  std::string path_to_scalar_time_file = dump_folder + "/" + bname + "-interface.scalar_time";
  std::ofstream scalar_time_file;
  if (world_rank == 0) {
    scalar_time_file.open(path_to_scalar_time_file, std::ofstream::out);
    scalar_time_file << std::scientific << std::setprecision(10);
    scalar_time_file << 0 << " " << 0.0 << std::endl;
  }

  std::string folder_name = dump_folder + "/" + bname + "-interface-DataFiles/";

  std::string path_to_moment_rate_file = folder_name + "moment_rate.out";
  std::ofstream moment_rate_file;
  double moment_rate = 0.0;
  if (world_rank == 0) {
    moment_rate_file.open(path_to_moment_rate_file, std::ios::out | std::ios::binary);
    moment_rate_file.write((char *)&moment_rate, sizeof(double));
  }

  std::string path_to_ED_file = folder_name + "ED.out";
  std::ofstream ED_file;
  double ED = 0.0;
  if (world_rank == 0) {
    ED_file.open(path_to_ED_file, std::ios::out | std::ios::binary);
    ED_file.write((char *)&ED, sizeof(double));
  }

  std::string path_to_ERR1_file = folder_name + "ERR1.out";
  std::ofstream ERR1_file;
  double ERR1 = 0.0;
  if (world_rank == 0) {
    ERR1_file.open(path_to_ERR1_file, std::ios::out | std::ios::binary);
    ERR1_file.write((char *)&ERR1, sizeof(double));
  }

  std::string path_to_ERR2_file = folder_name + "ERR2.out";
  std::ofstream ERR2_file;
  double ERR2 = 0.0;
  if (world_rank == 0) {
    ERR2_file.open(path_to_ERR2_file, std::ios::out | std::ios::binary);
    ERR2_file.write((char *)&ERR2, sizeof(double));
  }

  // ---------------------------------------------------------------------------

  // get displacement to check when to stop

  HalfSpace &top = interface.getTop();
  // HalfSpace& bot = interface.getBot();
  NodalField * top_disp_0 = top.getDisp(0);
  NodalField *velo0_top = top.getVelo(0);
  NodalField *velo2_top = top.getVelo(2);
  NodalField *tau0 = interface.getCohesion(0);
  NodalField *tau2 = interface.getCohesion(2);
  NodalField taui(mesh.getNbNodes());
  for (int i = 0; i < mesh.getNbNodes(); ++i) {
    taui(i) = std::sqrt((*tau0)(i) * (*tau0)(i) + (*tau2)(i) * (*tau2)(i));
  }

  // time stepping
  for (unsigned int s=1; s<=nb_time_steps; ++s) {
    if (world_rank==0) {
      std::cout << "s=" << s << "/" << nb_time_steps << "\r";
      std::cout.flush();
    }

    // nucleation
    double d_lim = nuc_speed * s * time_step;
    d_lim = std::min(d_lim, 2 * nuc_half_size - d_lim);
    d_lim = std::max(0., d_lim); // after seed crack is of zero length: do nothing

    for (int i = 0; i < mesh.getNbNodes(); ++i) {
      double x = std::abs((*coords[0])(i)-length / 2 - a * 0.7);
      double z = std::abs((*coords[2])(i)-length / 2 - a * 0.7);
      double r = std::sqrt(x * x + z * z);
      if (r > nuc_half_size) {
        // do nothing outside of seed crack area
      } else if (r > d_lim) { // set to initial value
	      // (*tau_c_p)(n) = tau_c;
	      // (*G_c_p)(n) = Gc;
      } else if (r < d_lim - nuc_w_zone) { // set to residual value
        (*tau_c_p)(i) = tau_r;
      } else{ // set the transient value within the process zone
        //double tau_slope = std::abs((*ext_normal)(n)) * ((*mu_s_f)(n) - (*mu_k_f)(n)) / nuc_w_zone;
        double tau_slope = (tau_c - tau_r) / nuc_w_zone;
        double G_slope = Gc / nuc_w_zone;
        (*tau_c_p)(i) = tau_slope * x + tau_c - tau_slope * d_lim;
        (*G_c_p)(i) = G_slope * x  + (Gc - G_slope * d_lim);
      }
    }

    // time integration
    interface.advanceTimeStep();

    // dump
    if (world_rank == 0)
      if (s % dump_int == 0 || s == nb_time_steps)
        interface.dump(s, s * time_step);

    if (world_rank == 0) {
      moment_rate = 0.0;
      for (int i = 0; i < mesh.getNbNodes(); ++i) {
        double vx = (*velo0_top)(i);
        double vz = (*velo2_top)(i);
        double v = std::sqrt(vx * vx + vz * vz) * 2.0;
        double taux = (*tau0)(i);
        double tauz = (*tau2)(i);
        double tau = std::sqrt(taux * taux + tauz * tauz);
        moment_rate += v * dx * dz * mat.getShearModulus();
        ED += v * tau * dx * dz * time_step;
        ERR1 += v * (tau - taui(i)) * dx * dz * time_step;
        ERR2 += v * tau * dx * dz * time_step;
      }
      scalar_time_file << s << " " << s * time_step << std::endl;
      moment_rate_file.write((char *)&moment_rate, sizeof(double));
      ED_file.write((char *)&ED, sizeof(double));
      ERR1_file.write((char *)&ERR1, sizeof(double));
      ERR2_file.write((char *)&ERR2, sizeof(double));
    }

    // check if all slides
    double min_disp = std::numeric_limits<double>::max();
    for (unsigned int n = 0; n < nb_nodes; ++n) {
      min_disp = std::min(min_disp, std::abs(top_disp_0->at(n)));
    }
    
    int done = false;
    if (world_rank == 0 && min_disp > 0.5 * 1e-6) {
	    std::cout << "Entire interface is broken! (min disp = " << min_disp << ")" << std::endl;
      done = true;
    }
    StaticCommunicatorMPI::getInstance()->broadcast(&done, 1);//so that everybody breaks
    if (done) break;
  }

  StaticCommunicatorMPI::getInstance()->finalize();

  if (world_rank == 0) {
    scalar_time_file.close();
    moment_rate_file.close();
    ED_file.close();
    ERR1_file.close();
    ERR2_file.close();
    std::cout << "weak-interface simulation completed." << std::endl;
  }
    

  return 0;
}
