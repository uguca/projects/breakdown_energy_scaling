# Earthquake Breakdown Energy Scaling Simulations

Simulations for the scaling of seismologically estimated earthquake breakdown energy.

# Getting Started
## Obtaining uguca

These simulations were implemented and executed on `uguca v0.9`. 

```
git clone git@gitlab.com:uguca/uguca.git -b v0.9
```

Please visit <https://uguca.gitlab.io/uguca/> for more details. 

## Clone this repository into `simulations`
```
cd uguca
mkdir simulations
cd simulations
git clone git@gitlab.com:uguca/projects/breakdown_energy_scaling.git
```

## Add this simulation into `cmake`
```
echo "add_subdirectory(breakdown_energy_scaling)" >> CMakeLists.txt
```

## Compile using `cmake`
```
cd ..
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DUCA_SIMULATIONS=ON  ..
cmake --build .
```

## Run simulation
First, go to where the executables are. 
```
cd simulations/breakdown_scaling_simulations
```
Create the folder for simulation output.
```
mkdir sim
```
Execute the standard simulation with the example input file `rupterm3d_098.in`.
```
mpirun rupterm3d rupterm3d_098.in
```
Or execute the off-centered nucleation simulation with the example input file `rupterm3d_114.in`.
```
mpirun rupterm3d_offnuc rupterm3d_114.in
```

***Remember*** to change `dump_folder` in `*.in` input files to a valid path, e.g., 
```
string dump_folder = /cluster/work/cmbm/ck659/
```

## Simulations
Input files of relevant simulations are included in this repository (`rupterm3d_***.in`), and their input and extracted output parameters are tabulated in `simulations.xlsx`. Figure 2 and Figure 3 in the manuscript can be reproduced using the data in `simulations.xlsx`. Note that `rupterm3d_114.in` is meant to be executed with `rupterm3d_offnuc.cc`, while the others are for `rupterm3d.cc`. 

|    Job Name   | Scaling Case | Scale ($`\chi`$) |
|:-------------:|:------------:|:----------------:|
| rupterm3d_098 |       A      |        1/4       |
| rupterm3d_099 |       A      |        1/2       |
| rupterm3d_100 |       A      |         1        |
| rupterm3d_101 |       A      |         2        |
| rupterm3d_102 |       A      |         4        |
| rupterm3d_103 |       A      |         8        |
| rupterm3d_104 |       A      |        16        |
| rupterm3d_106 |       B      |        1/4       |
| rupterm3d_107 |       B      |        1/2       |
| rupterm3d_108 |       B      |         1        |
| rupterm3d_109 |       B      |         2        |
| rupterm3d_110 |       B      |         4        |
| rupterm3d_111 |       B      |         8        |
| rupterm3d_112 |       B      |        16        |
| rupterm3d_114 |  A (offnuc)  |         4        |
